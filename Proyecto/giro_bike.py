
import cv2
import numpy as np
import timeit
import time
import sys

def Detect (imgi):
	hsv = cv2.cvtColor(imgi,cv2.COLOR_BGR2HSV)
	h,w,c = hsv.shape 
	hsv_max=np.arange(3)
	hsv_min=np.arange(3)
	hsv_max[0] = cv2.getTrackbarPos('Hmax','image')
	hsv_min[0] = cv2.getTrackbarPos('Hmin','image')
	hsv_max[1] = cv2.getTrackbarPos('Smax','image')
	hsv_min[1] = cv2.getTrackbarPos('Smin','image')
	hsv_max[2] = cv2.getTrackbarPos('Vmax','image')
	hsv_min[2] = cv2.getTrackbarPos('Vmin','image')
	mask = cv2.inRange(hsv, hsv_min, hsv_max)
	mask1 = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, np.ones((20,20),np.uint8))            
	res2 = cv2.bitwise_and(hsv, hsv, mask = mask1)
	gray=cv2.cvtColor(res2, cv2.COLOR_BGR2GRAY)
	res, thresh = cv2.threshold(gray,0,1,cv2.THRESH_BINARY)
	#thresh = cv2.medianBlur(thresh,3)
	contorno,hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
	maxArea=0
	contorno_mayor=0
	if (len(contorno)>0):
		for i in range (len(contorno)):  
			area = cv2.contourArea(contorno[i])
			if (area > maxArea):
				maxArea = area
				#print (maxArea, "area izquierda")
				contorno_mayor = i
	else:
		return imgi
	cv2.drawContours(imgi,contorno[contorno_mayor],-1,(0,255,0),5)
	M = cv2.moments(contorno[contorno_mayor])
	area = M['m00']
	if (area!=0 and 200000>area>conf.area): #no se debe detectar el cielo
		cx = M['m10']/M['m00']
		cy = M['m01']/M['m00']
		print 'centro masa', cx
		print 'area total', area
		if (cx < conf.cx_izq_max) and (cx > conf.cx_izq_min):
			contador.izq += 1
		elif contador.izq > 0:
			contador.izq -= 1
		if contador.izq > 2:
			contador.stop_izq = time.time() + 5
			contador.izq = 0
		if (cx < conf.cx_der_max) and (cx > conf.cx_der_min):
			contador.der += 1
		elif contador.der > 0:
			contador.der -= 1
		if contador.der > 2:
			contador.stop_der = time.time() + 5
			contador.der = 0
	return imgi

def nothing(x):
	pass

Ruta = sys.argv[1]
Presets = Ruta + '/setpoint.txt'
config = open(Presets,'r')
conf_text = config.read()
vect = conf_text.split('\n')

class conf:
	cx_der_max = int(vect[4])
	cx_der_min = int(vect[3])
	cx_izq_max = int(vect[7])
	cx_izq_min = int(vect[6])
	area = int(vect[9])


cv2.namedWindow('image')
cv2.namedWindow('frame')
cv2.createTrackbar('Hmax','image',0,180,nothing)
cv2.createTrackbar('Hmin','image',0,180,nothing)
cv2.createTrackbar('Smax','image',0,255,nothing)
cv2.createTrackbar('Smin','image',0,255,nothing)
cv2.createTrackbar('Vmax','image',0,255,nothing)
cv2.createTrackbar('Vmin','image',0,255,nothing)

cap = cv2.VideoCapture(Ruta + '/video.avi')
font = cv2.FONT_HERSHEY_SIMPLEX
manual = 0
detener = 0
ret, img=cap.read()
mean =0
img = cv2.resize(img, (1920,1080))
h,w,c = img.shape
mask_1080 = cv2.imread('mask1080.png')

print "\r\n ruta de acceso al video", (Ruta)
print conf_text
print "archivo de configuracion", (Presets), "\r\n"

class contador:
	izq = 0
	der = 0
	stop_der = 0 
	stop_izq = 0

while(1):

	#img = cv2.imread('arm_izq.jpg')
	if not ret:
		print("No se puede leer el archivo...")
		break
	calculo_color = img[int(h*3/4-100)-40:int(h*3/4-100)+40,w/2-40:w/2+40]
	calculo_color = cv2.resize(calculo_color, (400,400))
	cv2.imshow('image',calculo_color)
	calculo_color = cv2.cvtColor(calculo_color,cv2.COLOR_BGR2HSV)
	mean = cv2.mean(calculo_color)
	if manual == 0:
		cv2.setTrackbarPos('Hmax','image',int(mean[0])+10)
		cv2.setTrackbarPos('Hmin','image',int(mean[0])-10)
		cv2.setTrackbarPos('Smax','image',int(mean[1])+30)
		cv2.setTrackbarPos('Smin','image',int(mean[1])-30)
		cv2.setTrackbarPos('Vmax','image',int(mean[2])+40)
		cv2.setTrackbarPos('Vmin','image',int(mean[2])-40)
	zona_interes = cv2.bitwise_and(img, mask_1080)
	#zona_interes = img
	#hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
	frame1=Detect(zona_interes.copy())
	if (time.time()<contador.stop_der):
		cv2.putText(frame1,'brazo derecho',(800,30), font, 1,(255,255,255),2)
	if (time.time()<contador.stop_izq):
		cv2.putText(frame1,'brazo izquierdo',(10,30), font, 1,(255,255,255),2)
	cv2.imshow('frame',frame1)
	k = cv2.waitKey(50) & 0xFF

	if k == 27:
		break
	if k == 120:
		if detener == 1:
			detener = 0
		else:
			detener = 1
	if k == 99:
		if manual == 1:
			manual = 0
		else:
			manual = 1
	if detener == 0:
		ret, img=cap.read()
		img = cv2.resize(img, (1920,1080))

cap.release()
cv2.destroyAllWindows()